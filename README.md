# README #

This README file will explain how to build a **dice_game** binary and its unit tests.
It will also explain how to run it.

### What is this repository for? ###

* Dice Game is program that simulates rolling of two 6-sides dice.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Prerequisites ###
* git 2.5.0 or higher
* g++ compiler 4.3 or higher
* Cmake 2.6 or higher

### How to compile ###
* Clone this repository
* Navigate to the root directory
* Create **build** directory
* Navigate to the "build" directory
* Run **cmake ..** command
* Run **make** command

### How to run the binary ###
* Navigate to the **build** directory
* Run **dice_game <*file_name*>**

### How to run unit tests ###
* Navigate to the **build/src/unittests** directory
* Run **unittest** binary