#include "../dice.hpp"

#include <iostream>
#include <assert.h>
#include <string>

namespace src {
namespace unittests {

void test_default_constructor()
{
    std::string result;
    try
    {
        src::dice dice_one;
    }
    catch (...)
    {
        result = "ERROR";
    }
    assert(result.empty());
}

void test_copy_constructor()
{
    src::dice dice_one;
    dice_one.set_loaded_amount(2);
    dice_one.set_loaded_side(3);

    src::dice dice_two(dice_one);
    assert(dice_two.get_loaded_amount()== 2);
    assert(dice_two.get_loaded_side()== 3);
}

void test_assignment_operator()
{
    src::dice dice_one;
    dice_one.set_loaded_amount(2);
    dice_one.set_loaded_side(3);

    src::dice dice_two;
    dice_two = dice_one;
    assert(dice_two.get_loaded_amount()== 2);
    assert(dice_two.get_loaded_side()== 3);
}

void test_setters_getters()
{
    src::dice dice_one;
    dice_one.set_loaded_amount(2);
    dice_one.set_loaded_side(3);
    assert(dice_one.get_loaded_amount()== 2);
    assert(dice_one.get_loaded_side()== 3);
}

void test_throw_dice()
{
    src::dice dice_one;
    dice_one.set_loaded_amount(2);
    dice_one.set_loaded_side(3);
    dice_one.setup_probablity();

    dice_one.throw_dice();
    for (int i = 0; i < 1000000; ++i)
    {
        int side = dice_one.throw_dice();
        assert(side >=1 && side <= 6);
    }
}

}
}


