#include "dice_test.hpp"
#include "statistician_test.hpp"

int main()
{
    using namespace src::unittests;
    //test dice
    test_default_constructor();
    test_copy_constructor();
    test_assignment_operator();
    test_setters_getters();
    test_throw_dice();

    //test statistician
    test_constructor();
    test_store_roll();
    test_print_statistics();

    return 0;
}
