#include "../statistician.hpp"

#include <iostream>
#include <assert.h>
#include <string>
#include <stdlib.h>

namespace src {
namespace unittests {

void test_constructor()
{
    std::string result;
    try
    {
        src::statistician stats(6, 2);
    }
    catch (...)
    {
        result = "ERROR";
    }
    assert(result.empty());
}

void test_store_roll()
{
    //generate random seed
    time_t current_time;
    time(&current_time);
    srand(static_cast<int>(current_time));

    std::string result;
    try
    {
        src::statistician stats(6, 2);

        for (int i = 0; i < 1000000; ++i)
        {
            int side_one = rand() % 6 + 1;
            int side_two = rand() % 6 + 1;
            stats.store_roll(1, side_one);
            stats.store_roll(2, side_two);
        }
    }
    catch (...)
    {
        result = "ERROR";
    }
    assert(result.empty());
}

void test_print_statistics()
{
    //generate random seed
    time_t current_time;
    time(&current_time);
    srand(static_cast<int>(current_time));

    std::string result;
    try
    {
        src::statistician stats(6, 2);

        for (int i = 0; i < 1000000; ++i)
        {
            int side_one = rand() % 6 + 1;
            int side_two = rand() % 6 + 1;
            stats.store_roll(1, side_one);
            stats.store_roll(2, side_two);
        }
        stats.print_statistics(1);
        stats.print_statistics(2);
    }
    catch (...)
    {
        result = "ERROR";
    }
    assert(result.empty());
}

}
}


