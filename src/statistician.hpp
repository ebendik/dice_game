#ifndef STATISTICIAN_HPP_
#define STATISTICIAN_HPP_

#include <vector>

namespace src {

/*
 * class 'statistician' is used to give
 * statistics for a game of dice
 */
class statistician
{
public:
    statistician(int num_of_dice_sides, int num_of_dice);

    /*
     * function 'store_roll' is used to store
     * a value of a roll for a specific dice
     */
    void store_roll(int dice_num, int side);

    /*
     * function 'print_statistics' prints
     * statistics of a game
     */
    void print_statistics(int dice_num);

private:
    statistician(const statistician& other);
    statistician& operator=(const statistician& other);

private:
    int _num_of_sides;
    int _total_num_of_rolls;
    std::vector<std::vector<int> > _dice;
};

}

#endif
