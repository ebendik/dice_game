#ifndef KEY_IDENTIFIERS_HPP_
#define KEY_IDENTIFIERS_HPP_

#include <string>

namespace src {
std::string NUM_ROLLS = "NumRolls";
std::string DIE = "Die";
std::string LOADED_SIDE = "LoadedSide";
std::string LOADED_AMOUNT = "LoadAmount";
}

#endif
