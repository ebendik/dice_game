#ifndef DICE_HPP_
#define DICE_HPP_

#include <vector>

namespace src {

/*
 * class 'dice' is a representation
 * of a signle dice
 */
class dice
{
public:
    dice();

    dice(const dice& other);

    dice& operator=(const dice& other);

    /*
     * function 'set_loaded_side' is a setter
     */
    void set_loaded_side(int side);

    /*
     * function 'set_loaded_amount' is a setter
     */
    void set_loaded_amount(int amount);

    /*
     * function 'get_loaded_side' is a getter
     */
    int get_loaded_side() const;

    /*
     * function 'get_loaded_amount' is a getter
     */
    int get_loaded_amount() const;

    /*
     * function 'throw_dice' return a random
     * number for a dice throw
     */
    int throw_dice() const;

    /*
     * function 'setup_probablity' determines
     * probability for each side
     */
    void setup_probablity();

private:
    /*
     * function 'load_sides' load all of the sides
     * and their probability
     */
    void load_sides(double loaded_side_prob, double other_sides_prob);

private:
    static int NUM_SIDES;
    int _loaded_side;
    int _loaded_amount;
    typedef std::pair<int, double> side_prob_t;
    std::vector<side_prob_t> _dice_sides;
};

}
#endif
