#include "dice.hpp"

#include <stdlib.h>
#include <iostream>

namespace src {

int dice::NUM_SIDES = 6;

dice::dice()
        : _loaded_side(0), _loaded_amount(0)
{
}

dice::dice(const dice& other)
        : _loaded_side(other._loaded_side), _loaded_amount(other._loaded_amount), _dice_sides(other._dice_sides)
{
}

dice& dice::operator=(const dice& other)
{
    if (this != &other)
    {
        _loaded_side = other._loaded_side;
        _loaded_amount = other._loaded_amount;
        _dice_sides = other._dice_sides;
    }
    return *this;
}

void dice::set_loaded_side(int side)
{
    _loaded_side = side;
}

void dice::set_loaded_amount(int amount)
{
    _loaded_amount = amount;
}

int dice::get_loaded_side() const
{
    return _loaded_side;
}

int dice::get_loaded_amount() const
{
    return _loaded_amount;
}

int dice::throw_dice() const
{
    //get a percentage
    int result = rand() % 100;
    int random_side = 1;
    if (result <= _dice_sides.at(0).second)
    {
        random_side = _loaded_side;
    }
    else if (result < _dice_sides.at(1).second)
    {
        random_side = _dice_sides.at(1).first;
    }
    else if (result < _dice_sides.at(2).second)
    {
        random_side = _dice_sides.at(2).first;
    }
    else if (result < _dice_sides.at(3).second)
    {
        random_side = _dice_sides.at(3).first;
    }
    else if (result < _dice_sides.at(4).second)
    {
        random_side = _dice_sides.at(4).first;
    }
    else if (result < _dice_sides.at(5).second)
    {
        random_side = _dice_sides.at(5).first;
    }

    return random_side;
}

void dice::setup_probablity()
{
    //set up probability percentage
    //each side gets at start equal amount of percentage
    double percentage_breakdown = double(100) / NUM_SIDES;

    //we give bias to the loaded side
    double loaded_side_prob = percentage_breakdown * _loaded_amount;
    if (loaded_side_prob > 100)
    {
        loaded_side_prob = 100;
    }

    //we recalculate how much percentage other sides get
    double other_sides_prob = (100 - loaded_side_prob) / 5;

    //setup dice sides
    load_sides(loaded_side_prob, other_sides_prob);
}

void dice::load_sides(double loaded_side_prob, double other_sides_prob)
{
    for (int i = 1; i <= NUM_SIDES; ++i)
    {
        int index = i - 1;

        //use loaded_side_prob at index 0
        if (i == 1)
        {
            _dice_sides.push_back(side_prob_t(i, loaded_side_prob));
        }
        else
        {
            double percentage = loaded_side_prob + (other_sides_prob * (index));
            _dice_sides.push_back(side_prob_t(i, percentage));
        }

        //make sure the loaded side is at index 0
        if (_loaded_side != 1 && _loaded_side == i)
        {
            //move probability side to index 0
            std::swap(_dice_sides.at(0), _dice_sides.at(index));

            //update both sides probability
            _dice_sides.at(0).second = loaded_side_prob;

            double percentage = loaded_side_prob + (other_sides_prob * (index));
            _dice_sides.at(index).second = percentage;
        }
    }
}

}
