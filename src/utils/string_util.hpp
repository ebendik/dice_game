#ifndef STRING_UTIL_HPP_
#define STRING_UTIL_HPP_

#include <string>
#include <sstream>

namespace src {
namespace utils {

/*
 * function 'get_token' return a sub-string
 */
std::string get_token(const std::string& pair, std::size_t position, std::size_t length)
{
    return pair.substr(position, length);
}

/*
 * function 'parse_line' will returned a key=value pair
 */
template<typename T>
T parse_line(const std::string& file_line, const char delimiter)
{
    std::size_t delimiter_index = file_line.find(delimiter);
    if (delimiter_index == std::string::npos)
    {
        throw std::runtime_error("Invalid parameter: " + file_line);
    }

    //get key
    std::string key = get_token(file_line, 0, delimiter_index);

    //get value
    std::string value = get_token(file_line, delimiter_index + 1, std::string::npos);

    return T(key, value);
}

/*
 * function 'string_to_int' converts a string
 * object into an 'int'
 */
int string_to_int(const std::string& value)
{
    int result = 0;
    std::istringstream iss(value);
    iss >> result;
    return result;
}

}
}

#endif
