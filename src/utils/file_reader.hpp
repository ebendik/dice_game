#ifndef FILE_READER_HPP_
#define FILE_READER_HPP_

#include <fstream>

namespace src {
namespace utils {

/*
 * class 'file_reader' is used for
 * reading text files
 */
class file_reader
{
public:
    file_reader();

    file_reader(const std::string& file_name);

    ~file_reader();

    /*
     * function 'close_file' will close
     * an input stream
     */
    void close_file();

    /*
     * function 'open_file' will open a
     * text file
     */
    void open_file(const std::string& file_name);

    /*
     * function 'read_line' reads a single
     * line from a text file
     */
    void read_line(std::string& file_line);

    /*
     * function 'has_next_line' determines
     * if there is another line to read
     */
    bool has_next_line();

private:
    file_reader(const file_reader&);
    file_reader& operator=(const file_reader&);

private:
    std::ifstream _file_stream;
};
}
}
#endif
