#include "file_reader.hpp"

namespace src {
namespace utils {

file_reader::file_reader()
{
}

file_reader::file_reader(const std::string& file_name)
        : _file_stream(file_name.c_str())
{
}

file_reader::~file_reader()
{
    close_file();
}

void file_reader::close_file()
{
    if (_file_stream.is_open())
    {
        _file_stream.close();
    }
}

void file_reader::open_file(const std::string& file_name)
{
    _file_stream.open(file_name.c_str());
}

void file_reader::read_line(std::string& file_line)
{
    if (_file_stream.is_open())
    {
        std::getline(_file_stream, file_line);
    }
}

bool file_reader::has_next_line()
{
    if (_file_stream.is_open() && !_file_stream.eof())
    {
        return true;
    }

    return false;
}

}
}
