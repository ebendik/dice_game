#include "key_identifiers.hpp"
#include "dice.hpp"
#include "statistician.hpp"
#include "utils/file_reader.hpp"
#include "utils/string_util.hpp"

#include <iostream>
#include <utility>
#include <stdlib.h>

namespace src {

/*
 * class 'dice_game' is responsible for running the
 * dice game
 */
class dice_game
{
    typedef std::pair<std::string, std::string> keyval_t;
public:
    dice_game(const std::string& file_name)
            : _freader(file_name), _delimiter('='), _num_of_rolls(0), _current_dice(NULL), _statistician(6, 2)
    {
    }

    void run()
    {
        get_game_instructions();

        //throw both dice
        for (int i = 1; i <= _num_of_rolls; ++i)
        {
            throw_dice(i);
        }
        std::cout << std::endl;

        print_statistics();
    }

private:
    /*
     * function 'get_game_instructions' retrieves game instructions
     * from a text file and uses a utility function to
     * parse each instruction into a key=value pair
     */
    void get_game_instructions()
    {
        std::string file_line;
        while (_freader.has_next_line())
        {
            _freader.read_line(file_line);
            if (!file_line.empty())
            {
                //parse line into key value pair
                keyval_t key_val = utils::parse_line<keyval_t>(file_line, _delimiter);

                //set parameter for the newly parsed key value pair
                set_parameter(key_val);
            }
        }

        //assert that all of the required parameters are loaded
        if (!required_parameters_set())
        {
            throw std::runtime_error("One ore more parameters required to run the Dice Game is not set!");
        }

        //set probablity
        _dice_one.setup_probablity();
        _dice_two.setup_probablity();
    }

    /*
     * function 'set_parameter' takes in a
     * key=value pair and assigns it to a correct party
     */
    void set_parameter(const keyval_t& key_val)
    {
        if (key_val.first == NUM_ROLLS)
        {
            _num_of_rolls = utils::string_to_int(key_val.second);
        }
        else if (key_val.first == DIE)
        {
            //set current dice
            if (key_val.second == "Die1")
            {
                _current_dice = &_dice_one;
            }
            else
            {
                _current_dice = &_dice_two;
            }
        }
        else
        {
            set_parameter(key_val, _current_dice);
        }
    }

    /*
     * function 'set_parameter' is a helper function
     */
    void set_parameter(const keyval_t& key_val, dice* current_dice)
    {
        if (current_dice && key_val.first == LOADED_SIDE)
        {
            int side = utils::string_to_int(key_val.second);
            current_dice->set_loaded_side(side);
        }
        else if (current_dice && key_val.first == LOADED_AMOUNT)
        {
            int amount = utils::string_to_int(key_val.second);
            current_dice->set_loaded_amount(amount);
        }
    }

    /*
     * function 'required_parameters_set' makes sure
     * that all of the required parameters are set
     */
    bool required_parameters_set()
    {
        if (_num_of_rolls <= 0)
        {
            return false;
        }

        return required_parameters_set(_dice_one) && required_parameters_set(_dice_two);
    }

    /*
     * function 'required_parameters_set' is a helper function
     */
    bool required_parameters_set(const dice& current_dice)
    {
        if (current_dice.get_loaded_side() <= 0 || current_dice.get_loaded_amount() <= 0)
        {
            return false;
        }

        return true;
    }

    /*
     * function 'throw_dice' throws both dice
     */
    void throw_dice(int throw_num)
    {
        int val_one = _dice_one.throw_dice();
        int val_two = _dice_two.throw_dice();
        std::cout << "Throw " << throw_num << ": Die1 rolled a " << val_one << ", ";
        std::cout << "Die2 rolled a " << val_two << std::endl;

        //store results in a statistician
        _statistician.store_roll(1, val_one);
        _statistician.store_roll(2, val_two);
    }

    void print_statistics()
    {
        _statistician.print_statistics(1);
        _statistician.print_statistics(2);
    }

private:
    utils::file_reader _freader;
    char _delimiter;
    dice _dice_one, _dice_two;
    int _num_of_rolls;
    dice* _current_dice;
    statistician _statistician;
}
;

}

int main(int argc, char* argv[])
{
    //generate random seed
    time_t current_time;
    time(&current_time);
    srand(static_cast<int>(current_time));

    //make sure user passes a file name
    if (argc < 2)
    {
        std::cout << "Must provide a file name with game instructions!" << std::endl;
        return 1;
    }

    try
    {
        const std::string file_name(argv[1]);
        src::dice_game dice_game(file_name);
        dice_game.run();
    }
    catch (const std::runtime_error& ex)
    {
        std::cout << "Program terminated with the following exception: " << ex.what() << std::endl;
    }
    catch (const std::out_of_range& ex)
    {
        std::cout << "Program terminated with the following exception: " << ex.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "Program terminated unexpectedly" << std::endl;
    }

    return 0;
}
