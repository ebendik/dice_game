#include "statistician.hpp"

#include <iostream>

namespace src {

statistician::statistician(int num_of_sides, int num_of_dice)
        : _num_of_sides(num_of_sides), _total_num_of_rolls(0)
{
    _dice.resize(num_of_dice);

    //resize each dice each dice statistic
    for (std::size_t i = 0; i < _dice.size(); ++i)
    {
        _dice.at(i).resize(_num_of_sides);
    }

}

void statistician::store_roll(int dice_num, int side)
{
    //increment the number of times a side was rolled
    //for a particular dice
    //we need to decrement dice_num and side
    //since they both start at '1'
    int& value = _dice.at(--dice_num).at(--side);
    ++value;

    //increment roll count
    ++_total_num_of_rolls;
}

void statistician::print_statistics(int dice_num)
{
    //we calculate the number of rolls for each dice,
    //by dividing total_num_of_rolls by number of dice
    int num_of_rolls = _total_num_of_rolls / _dice.size();

    //iterate over each side and calculate its percentage
    std::cout << "Die" << dice_num << " Statistics for " << num_of_rolls << ":" << std::endl;
    for (int i = 0; i < _num_of_sides; ++i)
    {
        double roll_amount = static_cast<double>(_dice.at(dice_num - 1).at(i));
        int percentage = (roll_amount / num_of_rolls) * 100;
        std::cout << "Side " << (i + 1) << ": " << percentage << "%" << std::endl;
    }
    std::cout << std::endl;
}

}
